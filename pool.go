/*
Package pool provides map backed LRU pool.
*/
package pool

import (
	"io"
	"sync"
	"time"
)

//Closabler is the interface that wraps Closable method.
type Closabler interface {
	Closable() bool
}

//Factory is the interface of the pool factory.
type Factory interface {
	New(key interface{}) (interface{}, error)
}

//Pool is the pool.
type Pool struct {
	Factory     Factory
	pool        map[interface{}]*entry
	closeNotify chan struct{}
	m           sync.Mutex
}

//CloseError represents error on call Close().
type CloseError struct {
	Item interface{}
	Err  error
}

//Error returns error message.
func (ce CloseError) Error() string {
	return "pool: could not close(reason: " + ce.Err.Error() + ")"
}

type entry struct {
	item       interface{}
	lastAccess time.Time
}

//New creates Pool.
func New(f Factory) *Pool {
	p := &Pool{
		Factory: f,
		pool:    make(map[interface{}]*entry),
	}
	return p
}

//Close disposes all items and finishes worker.
func (p *Pool) Close() error {
	err := p.Sweep(0, true)
	if err != nil {
		return err
	}

	p.m.Lock()
	cn := p.closeNotify
	p.m.Unlock()

	if cn != nil {
		cn <- struct{}{}
	}

	//wait worker
	p.m.Lock()
	defer p.m.Unlock()

	return nil
}

//GetExists gets item if it exists in pool.
func (p *Pool) GetExists(key interface{}) (interface{}, bool) {
	p.m.Lock()
	defer p.m.Unlock()

	e, ok := p.pool[key]
	if ok {
		e.lastAccess = time.Now()
		return e.item, true
	}

	return nil, false
}

//Get gets item.
//
//When an item does not exist, it creates via the Factroy.
func (p *Pool) Get(key interface{}) (interface{}, error) {
	p.m.Lock()
	defer p.m.Unlock()

	e, ok := p.pool[key]
	if !ok {
		item, err := p.Factory.New(key)
		if err != nil {
			return nil, err
		}

		e = &entry{
			item: item,
		}
		p.pool[key] = e
	}
	e.lastAccess = time.Now()
	return e.item, nil
}

//Sweep closes unused item.
//
//item does not close, when forceClose is false and item.Closable() is false.
func (p *Pool) Sweep(deadline time.Duration, forceClose bool) error {
	p.m.Lock()
	defer p.m.Unlock()

	var err error

	n := time.Now()
	for k, e := range p.pool {
		if n.Sub(e.lastAccess) < deadline {
			continue
		}

		if cl, ok := e.item.(io.Closer); ok {
			if !forceClose {
				if cls, ok := e.item.(Closabler); ok && !cls.Closable() {
					e.lastAccess = n
					continue
				}
			}

			err = cl.Close()
			if err != nil {
				return CloseError{
					Item: e.item,
					Err:  err,
				}
			}
		}

		delete(p.pool, k)
	}
	return nil
}

//StartWorker runs worker goroutine.
func (p *Pool) StartWorker(deadline time.Duration, interval time.Duration) <-chan error {
	p.m.Lock()
	defer p.m.Unlock()

	if p.closeNotify != nil {
		//Already running
		return nil
	}

	p.closeNotify = make(chan struct{})
	errch := make(chan error)

	go p.worker(deadline, interval, errch)

	return errch
}

func (p *Pool) worker(deadline time.Duration, interval time.Duration, errch chan<- error) {
	var err error
	for {
		select {
		case <-time.After(interval):
			err = p.Sweep(deadline, false)
			if err != nil {
				errch <- err
			}
		case <-p.closeNotify:
			return
		}
	}
}
