package pool

import (
	"errors"
	"os"
	"testing"
)

type FileFactory string

func (ff FileFactory) New(key interface{}) (interface{}, error) {
	s, ok := key.(string)
	if !ok {
		return nil, errors.New("crazy key type")
	}

	return os.Open(string(ff) + "/" + s)
}

func TestPool(t *testing.T) {
	pool := New(FileFactory("."))
	if pool == nil {
		t.Fail()
		return
	}

	r, err := pool.Get(123)
	if r != nil {
		t.Fail()
		return
	}
	if err == nil || err.Error() != "crazy key type" {
		t.Fail()
		return
	}

	r, ok := pool.GetExists("pool_test.go")
	if r != nil || ok {
		t.Fail()
		return
	}

	r1, err := pool.Get("pool_test.go")
	if err != nil {
		t.Error(err)
		return
	}
	if _, ok := r1.(*os.File); !ok {
		t.Fail()
	}

	r2, ok := pool.GetExists("pool_test.go")
	if !ok {
		t.Fail()
		return
	}
	if _, ok := r2.(*os.File); !ok {
		t.Fail()
	}

	if r1 != r2 {
		t.Fail()
		return
	}

	err = pool.Sweep(0, false)
	if err != nil {
		t.Error(err)
		return
	}

	r, ok = pool.GetExists("pool_test.go")
	if r != nil || ok {
		t.Fail()
		return
	}
}

type ClosablerTesterFactory bool

func (ctf ClosablerTesterFactory) New(key interface{}) (interface{}, error) {
	return ClosablerTester(ctf), nil
}

type ClosablerTester bool

func (ct ClosablerTester) Close() error {
	return nil
}

func (ct ClosablerTester) Closable() bool {
	return bool(ct)
}

func TestClosabler(t *testing.T) {
	pool := New(ClosablerTesterFactory(false))
	_, err := pool.Get("test")
	if err != nil {
		t.Error(err)
		return
	}

	err = pool.Sweep(0, false)
	if err != nil {
		t.Error(err)
		return
	}

	_, ok := pool.GetExists("test")
	if !ok {
		t.Fail()
	}

	err = pool.Sweep(0, true)
	if err != nil {
		t.Error(err)
		return
	}

	_, ok = pool.GetExists("test")
	if ok {
		t.Fail()
	}
}
